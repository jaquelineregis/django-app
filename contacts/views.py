from django.shortcuts import render
from django.views import generic
from .models import Contact


class ContactTemplateView(generic.TemplateView):
    template_name = "contacts/options.html"


class ContactListView(generic.ListView):
    model = Contact
    template_name = 'contacts/list.html'


class ContactCreateView(generic.CreateView):
    model = Contact
    fields = ['name', 'email']
    template_name = 'contacts/add.html'
    success_url='/list'
