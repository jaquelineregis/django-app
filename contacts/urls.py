from django.urls import path
from . import views


app_name = 'contacts'

urlpatterns = [
    path('', views.ContactTemplateView.as_view(), name='index'),
    path('list/', views.ContactListView.as_view(), name='list'),
    path('add/', views.ContactCreateView.as_view(), name='add'),
]
