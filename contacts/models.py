from django.db import models
from django.utils.translation import gettext as _


class Contact(models.Model):
    name = models.CharField(_('Name'), max_length=50)
    email = models.EmailField(_('email'))

    def __str__(self):
        return self.name
